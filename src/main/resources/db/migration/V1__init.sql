CREATE TABLE orders
(
    id serial primary key,
    submit_deadline timestamp not null,
    paid boolean
);

CREATE TABLE products
(
    id serial primary key,
    name varchar not null,
    stock_quantity numeric not null,
    price numeric not null
);

CREATE TABLE order_details
(
    id serial primary key,
    order_id bigint not null,
    product_id bigint not null,
    quantity numeric not null,
    constraint fk_order_id foreign key (order_id) references orders(id),
    constraint fk_product_id foreign key (product_id) references products(id)
);

CREATE INDEX index_name ON orders (submit_deadline, paid);

insert into products (name, stock_quantity, price) VALUES ('rohlík', 90, 5);
insert into products (name, stock_quantity, price) VALUES ('mléko', 10, 2);
