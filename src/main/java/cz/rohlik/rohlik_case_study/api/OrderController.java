package cz.rohlik.rohlik_case_study.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.rohlik.rohlik_case_study.api.model.OrderCreateRequest;
import cz.rohlik.rohlik_case_study.api.model.OrderCreateResponse;
import cz.rohlik.rohlik_case_study.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Tag(name = "Order apis", description = "Order management apis")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;

    @Operation(summary = "Creates new order")
    @PostMapping("/order")
    public ResponseEntity<OrderCreateResponse> createOrder(@RequestBody OrderCreateRequest order) throws JsonProcessingException {
        return ResponseEntity.ok(orderService.createOrder(order));
    }

    @Parameter(name = "orderId", description = "Id of order to delete")
    @Operation(summary = "Deletes order")
    @DeleteMapping("/order/{orderId}")
    public void deleteOrder(@PathVariable("orderId") Long orderId) {
        orderService.deleteOrder(orderId);
    }

    @Parameter(name = "orderId", description = "Id of order to pay")
    @Operation(summary = "Adds payed flag to the order")
    @GetMapping("/order/payment/{orderId}")
    public void payment(@PathVariable("orderId") Long orderId) {
        orderService.orderPayment(orderId);
    }
}
