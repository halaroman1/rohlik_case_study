package cz.rohlik.rohlik_case_study.api.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderedProduct {
    @Schema(name = "Product Id", example = "1")
    private Long productId;
    @Schema(name = "Quantity of ordered product", example = "2")
    private Long quantity;
}
