package cz.rohlik.rohlik_case_study.api.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderCreateResponse {
    @Schema(name = "Order Id", example = "1")
    private Long orderId;
    @Schema(name = "List of missing products (if requested quantity is greater then stock)")
    private List<MissingProduct> missingProducts;
}
