package cz.rohlik.rohlik_case_study.api.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Product {
    @Schema(name = "Name of product", example = "Rohlík")
    private String name;
    @Schema(name = "Stock quantity", example = "10")
    private Long stockQuantity = 0L;
    @Schema(name = "Product price with quantity of 1", example = "1")
    private BigDecimal price;
}
