package cz.rohlik.rohlik_case_study.api.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class OrderCreateRequest {
    @Schema(name = "List of ordered products")
    private List<OrderedProduct> orderedProducts;
}
