package cz.rohlik.rohlik_case_study.api.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class ErrorResponse {
    @Schema(name = "Error message", example = "Product has orders and cannot be deleted")
    private String message;
}
