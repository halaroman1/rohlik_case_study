package cz.rohlik.rohlik_case_study.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.rohlik.rohlik_case_study.api.model.Product;
import cz.rohlik.rohlik_case_study.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@Tag(name = "Product apis", description = "Product management apis")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    @Operation(summary = "Creates new product")
    @PostMapping("/product")
    public Long createProduct(@RequestBody Product product) throws JsonProcessingException {
        return productService.createProduct(product);
    }

    @Operation(summary = "Updated product")
    @Parameter(name = "productId", description = "Id of product to update")
    @PutMapping("/product/{productId}")
    public void updateProduct(@PathVariable("productId") Long productId, @RequestBody Product product) throws JsonProcessingException {
        productService.updateProduct(productId, product);
    }

    @Operation(summary = "Deletes product")
    @Parameter(name = "productId", description = "Id of product to delete")
    @DeleteMapping("/product/{productId}")
    public void deleteProduct(@PathVariable("productId") Long productId) {
        productService.deleteProduct(productId);
    }
}
