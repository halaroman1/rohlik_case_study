package cz.rohlik.rohlik_case_study.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.rohlik.rohlik_case_study.api.model.MissingProduct;
import cz.rohlik.rohlik_case_study.api.model.OrderCreateRequest;
import cz.rohlik.rohlik_case_study.api.model.OrderCreateResponse;
import cz.rohlik.rohlik_case_study.repository.OrderRepository;
import cz.rohlik.rohlik_case_study.repository.ProductRepository;
import cz.rohlik.rohlik_case_study.repository.model.OrderDetailsEntity;
import cz.rohlik.rohlik_case_study.repository.model.OrderEntity;
import cz.rohlik.rohlik_case_study.repository.model.ProductEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;
    private final ObjectMapper objectMapper;

    @Transactional
    public OrderCreateResponse createOrder(OrderCreateRequest order) throws JsonProcessingException {
        List<MissingProduct> missingProducts = getMissingProducts(order);
        if (missingProducts.isEmpty()) {
            OrderEntity orderEntity = orderRepository.save(OrderEntity.builder()
                            .orderSubmitDeadlineDateTime(Instant.now().plusSeconds(60 * 30L))
                            .paid(false)
                            .build());
            List<OrderDetailsEntity> orderDetails = new ArrayList<>();
            order.getOrderedProducts().forEach(orderedProduct -> {
                ProductEntity productEntity = productRepository.findById(orderedProduct.getProductId()).orElseThrow();
                productEntity.setStockQuantity(productEntity.getStockQuantity() - orderedProduct.getQuantity());
                orderDetails.add(OrderDetailsEntity.builder()
                        .product(productRepository.save(productEntity))
                        .quantity(orderedProduct.getQuantity())
                        .order(orderEntity)
                        .build());
            });
            orderEntity.setOrderDetails(orderDetails);
            OrderEntity result = orderRepository.save(orderEntity);
            log.info("Created order: {}", objectMapper.writeValueAsString(orderEntity));
            return OrderCreateResponse.builder().orderId(result.getId()).build();
        } else {
            return OrderCreateResponse.builder().missingProducts(missingProducts).build();
        }
    }

    @Transactional
    public void deleteOrder(Long orderId) {
        OrderEntity orderEntity = orderRepository.findById(orderId).orElseThrow(() -> new IllegalStateException("Order not found with id " + orderId));
        orderEntity.getOrderDetails().forEach(orderDetailsEntity -> {
            ProductEntity productEntity = orderDetailsEntity.getProduct();
            productEntity.setStockQuantity(productEntity.getStockQuantity() + orderDetailsEntity.getQuantity());
            productRepository.save(productEntity);
        });
        orderRepository.deleteById(orderId);
        log.info("Deleted order with id {}", orderId);
    }

    @Transactional
    public void orderPayment(Long orderId) {
        OrderEntity orderEntity = orderRepository.findById(orderId).orElseThrow(() -> new IllegalStateException("Order not found with id " + orderId));
        if (orderEntity.getPaid()) {
            throw new IllegalStateException("Payment is already payed");
        }
        orderEntity.setPaid(true);
        orderRepository.save(orderEntity);
        log.info("Order with id {} is payed", orderId);
    }

    private List<MissingProduct> getMissingProducts(OrderCreateRequest order) {
        List<MissingProduct> invalidOrders = new ArrayList<>();
        order.getOrderedProducts().forEach(orderedProduct -> {
            ProductEntity product = productRepository.findById(orderedProduct.getProductId()).orElseThrow(() -> new IllegalStateException("Product not found with id " + orderedProduct.getProductId()));
            if (product.getStockQuantity() < orderedProduct.getQuantity()) {
                invalidOrders.add(new MissingProduct(orderedProduct.getProductId(), orderedProduct.getQuantity() - product.getStockQuantity()));
            }
        });
        return invalidOrders;
    }
}
