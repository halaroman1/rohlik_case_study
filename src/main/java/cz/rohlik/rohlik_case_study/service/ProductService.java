package cz.rohlik.rohlik_case_study.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.rohlik.rohlik_case_study.api.model.Product;
import cz.rohlik.rohlik_case_study.repository.ProductRepository;
import cz.rohlik.rohlik_case_study.repository.model.ProductEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final ObjectMapper objectMapper;

    public Long createProduct(Product product) throws JsonProcessingException {
        ProductEntity productEntity = productRepository.save(ProductEntity.builder()
                .price(product.getPrice())
                .name(product.getName())
                .stockQuantity(product.getStockQuantity())
                .build());
        log.info("Created product: {}", objectMapper.writeValueAsString(productEntity));
        return productEntity.getId();
    }

    @Transactional
    public void updateProduct(Long productId, Product product) throws JsonProcessingException {
        ProductEntity productEntity = productRepository.findById(productId).orElseThrow(() -> new IllegalStateException("Product not found with id " + productId));
        if (product.getPrice() != null) {
            productEntity.setPrice(product.getPrice());
        }
        if (product.getName() != null) {
            productEntity.setName(product.getName());
        }
        if (product.getStockQuantity() != null) {
            productEntity.setStockQuantity(product.getStockQuantity());
        }
        productRepository.save(productEntity);
        log.info("Updated product: {} to values: {}",
                objectMapper.writeValueAsString(productEntity),
                objectMapper.writeValueAsString(product));
    }

    public void deleteProduct(Long productId) {
        productRepository.deleteById(productId);
        log.info("Deleted product with id {}", productId);
    }
}
