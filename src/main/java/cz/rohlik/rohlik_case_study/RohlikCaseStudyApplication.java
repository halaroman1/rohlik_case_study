package cz.rohlik.rohlik_case_study;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class RohlikCaseStudyApplication {

    public static void main(String[] args) {
        SpringApplication.run(RohlikCaseStudyApplication.class, args);
    }

}
