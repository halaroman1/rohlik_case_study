package cz.rohlik.rohlik_case_study.scheduled;

import cz.rohlik.rohlik_case_study.repository.OrderRepository;
import cz.rohlik.rohlik_case_study.repository.ProductRepository;
import cz.rohlik.rohlik_case_study.repository.model.OrderEntity;
import cz.rohlik.rohlik_case_study.repository.model.ProductEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Component
@RequiredArgsConstructor
public class CancelUnpaidOrders {
    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;

    @Scheduled(fixedDelay = 60000, initialDelay = 60000)
    @Transactional
    public void cancelUnpaidOrders() {
        List<OrderEntity> unpaidOrders = orderRepository.findByPaidFalseAndOrderSubmitDeadlineDateTimeBefore(Instant.now());
        unpaidOrders.forEach(orderEntity -> {
            orderEntity.getOrderDetails().forEach(orderDetailsEntity -> {
                ProductEntity productEntity = productRepository.findById(orderDetailsEntity.getProduct().getId()).orElseThrow();
                productEntity.setStockQuantity(productEntity.getStockQuantity() + orderDetailsEntity.getQuantity());
                productRepository.save(productEntity);
            });
            orderRepository.delete(orderEntity);
        });
    }
}
