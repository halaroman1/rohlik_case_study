package cz.rohlik.rohlik_case_study.repository;

import cz.rohlik.rohlik_case_study.repository.model.OrderEntity;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<OrderEntity, Long> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    List<OrderEntity> findByPaidFalseAndOrderSubmitDeadlineDateTimeBefore(Instant now);
}
