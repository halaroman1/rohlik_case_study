package cz.rohlik.rohlik_case_study.repository.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "products")
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name = "product_name";

    @Column(name = "stock_quantity")
    private Long stockQuantity = 0L;

    @Column
    private BigDecimal price = BigDecimal.ZERO;
}
