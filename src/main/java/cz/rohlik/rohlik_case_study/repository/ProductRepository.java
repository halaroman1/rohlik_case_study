package cz.rohlik.rohlik_case_study.repository;

import cz.rohlik.rohlik_case_study.repository.model.ProductEntity;
import jakarta.persistence.LockModeType;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
    @Override
    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    Optional<ProductEntity> findById(@NonNull Long productId);
}
