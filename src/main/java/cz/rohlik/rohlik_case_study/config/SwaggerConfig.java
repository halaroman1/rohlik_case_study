package cz.rohlik.rohlik_case_study.config;

import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {
    @Bean
    GroupedOpenApi allApis() {
        return GroupedOpenApi.builder()
                .group("ALL")
                .pathsToMatch("/**")
                .build();
    }
}
