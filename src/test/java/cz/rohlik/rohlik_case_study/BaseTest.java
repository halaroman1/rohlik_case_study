package cz.rohlik.rohlik_case_study;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.rohlik.rohlik_case_study.repository.OrderRepository;
import cz.rohlik.rohlik_case_study.repository.ProductRepository;
import cz.rohlik.rohlik_case_study.scheduled.CancelUnpaidOrders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public abstract class BaseTest {
    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected OrderRepository orderRepository;
    @Autowired
    protected ProductRepository productRepository;
    @Autowired
    protected CancelUnpaidOrders cancelUnpaidOrders;

    protected final ObjectMapper objectMapper = new ObjectMapper();
}
