package cz.rohlik.rohlik_case_study.api;


import cz.rohlik.rohlik_case_study.BaseTest;
import cz.rohlik.rohlik_case_study.api.model.Product;
import cz.rohlik.rohlik_case_study.repository.model.OrderDetailsEntity;
import cz.rohlik.rohlik_case_study.repository.model.OrderEntity;
import cz.rohlik.rohlik_case_study.repository.model.ProductEntity;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ProductControllerTest extends BaseTest {
    @Test
    @Order(1)
    void createProduct_happyPath() throws Exception {
        Long productId = createProduct();

        ProductEntity productEntity = productRepository.findById(productId).orElseThrow();
        assertEquals(productEntity.getName(), "zmrzlina");
        assertEquals(productEntity.getPrice().doubleValue(), 20);
        assertEquals(productEntity.getStockQuantity(), 1000);
    }

    @Test
    @Order(2)
    void updateProduct_happyPath() throws Exception {
        Long productId = createProduct();
        Product request = Product.builder()
                .price(BigDecimal.valueOf(30))
                .name("zmrzlina-A")
                .stockQuantity(1L)
                .build();
        mockMvc.perform(put("/product/{productId}", productId)
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        ProductEntity productEntity = productRepository.findById(productId).orElseThrow();
        assertEquals(productEntity.getName(), "zmrzlina-A");
        assertEquals(productEntity.getPrice().doubleValue(), 30);
        assertEquals(productEntity.getStockQuantity(), 1);
    }

    @Test
    @Order(3)
    void deleteProduct_happyPath() throws Exception {
        Long productId = createProduct();

        mockMvc.perform(delete("/product/{productId}", productId))
                .andExpect(status().isOk()).andReturn();

        assertFalse(productRepository.findById(productId).isPresent());
    }

    @Test
    @Order(4)
    void deleteProduct_WithOrder_shouldFail() throws Exception {
        Long productId = createProduct();
        OrderEntity orderEntity = orderRepository.save(OrderEntity.builder()
                .orderSubmitDeadlineDateTime(Instant.now().plusSeconds(60 * 30L))
                .paid(false)
                .build());
        ProductEntity productEntity = productRepository.findById(productId).orElseThrow();
        productEntity.setStockQuantity(productEntity.getStockQuantity() - 1);
        OrderDetailsEntity orderDetailsEntity = OrderDetailsEntity.builder()
                .product(productRepository.save(productEntity))
                .quantity(1L)
                .order(orderEntity)
                .build();
        orderEntity.setOrderDetails(Collections.singletonList(orderDetailsEntity));
        orderRepository.save(orderEntity);

        MvcResult mvcResult = mockMvc.perform(delete("/product/{productId}", productId))
                .andExpect(status().isInternalServerError()).andReturn();

        assertTrue(mvcResult.getResponse().getContentAsString().contains("Product has orders and cannot be deleted"));
    }

    private Long createProduct() throws Exception {
        Product request = Product.builder()
                .price(BigDecimal.valueOf(20))
                .name("zmrzlina")
                .stockQuantity(1000L)
                .build();
        MvcResult result = mockMvc.perform(post("/product")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isOk()).andReturn();

        return objectMapper.readValue(result.getResponse().getContentAsString(), Long.class);
    }
}
