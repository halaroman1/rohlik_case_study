package cz.rohlik.rohlik_case_study.api;


import cz.rohlik.rohlik_case_study.BaseTest;
import cz.rohlik.rohlik_case_study.api.model.OrderCreateRequest;
import cz.rohlik.rohlik_case_study.api.model.OrderCreateResponse;
import cz.rohlik.rohlik_case_study.api.model.OrderedProduct;
import cz.rohlik.rohlik_case_study.repository.model.OrderEntity;
import cz.rohlik.rohlik_case_study.repository.model.ProductEntity;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class OrderControllerTest extends BaseTest {
    @Test
    @Order(1)
    void createOrder_happyPath() throws Exception {
        OrderCreateResponse orderCreateResponse = createOrder();

        assertNull(orderCreateResponse.getMissingProducts());
        OrderEntity orderEntity = orderRepository.findById(orderCreateResponse.getOrderId()).orElseThrow();
        assertEquals(orderEntity.getPaid(), false);
        assertEquals(orderEntity.getOrderDetails().size(), 1);
        assertEquals(orderEntity.getOrderDetails().get(0).getProduct().getId(), 1);
        assertEquals(orderEntity.getOrderDetails().get(0).getQuantity(), 1);
        ProductEntity productEntity = productRepository.findById(1L).orElseThrow();
        assertEquals(productEntity.getStockQuantity(), 89);
    }

    @Test
    @Order(2)
    void orderPayment_happyPath() throws Exception {
        createOrder();

        mockMvc.perform(get("/order/payment/{orderId}", "1"))
                .andExpect(status().isOk()).andReturn();

        OrderEntity orderEntity = orderRepository.findById(1L).orElseThrow();

        assertTrue(orderEntity.getPaid());
    }

    @Test
    @Order(3)
    void deleteOrder_happyPath() throws Exception {
        createOrder();

        mockMvc.perform(delete("/order/{orderId}", "1"))
                .andExpect(status().isOk()).andReturn();

        assertFalse(orderRepository.findById(1L).isPresent());
    }

    @Test
    @Order(4)
    void createOrder_badProductId_shouldFail() throws Exception {
        List<OrderedProduct> orderedProducts = new ArrayList<>();
        orderedProducts.add(new OrderedProduct(99L, 1L));
        OrderCreateRequest request = new OrderCreateRequest(orderedProducts);
        String response = mockMvc.perform(post("/order")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isInternalServerError()).andReturn().getResponse().getContentAsString();

        assertTrue(response.contains("Product not found with id 99"));
    }

    @Test
    @Order(5)
    void orderPayment_forAlreadyPayedOrder_shouldFail() throws Exception {
        createOrder();
        createOrder();

        mockMvc.perform(get("/order/payment/{orderId}", "2"))
                .andExpect(status().isOk()).andReturn();

        String response = mockMvc.perform(get("/order/payment/{orderId}", "2"))
                .andExpect(status().isInternalServerError()).andReturn().getResponse().getContentAsString();

        assertTrue(response.contains("Payment is already payed"));
    }

    private OrderCreateResponse createOrder() throws Exception {
        List<OrderedProduct> orderedProducts = new ArrayList<>();
        orderedProducts.add(new OrderedProduct(1L, 1L));
        OrderCreateRequest request = new OrderCreateRequest(orderedProducts);
        MvcResult result = mockMvc.perform(post("/order")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isOk()).andReturn();

        return objectMapper.readValue(result.getResponse().getContentAsString(), OrderCreateResponse.class);
    }
}
