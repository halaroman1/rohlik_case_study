package cz.rohlik.rohlik_case_study.scheduled;

import cz.rohlik.rohlik_case_study.BaseTest;
import cz.rohlik.rohlik_case_study.repository.model.OrderEntity;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CancelUnpaidOrdersTest extends BaseTest {
    @Test
    void cancelUnpaidOrders_happyPath() {
        OrderEntity orderEntity = orderRepository.save(OrderEntity.builder()
                .orderSubmitDeadlineDateTime(Instant.now().minusSeconds(1))
                .paid(false)
                .build());
        OrderEntity orderEntity2 = orderRepository.save(OrderEntity.builder()
                .orderSubmitDeadlineDateTime(Instant.now().plusSeconds(60))
                .paid(false)
                .build());
        OrderEntity orderEntity3 = orderRepository.save(OrderEntity.builder()
                .orderSubmitDeadlineDateTime(Instant.now().minusSeconds(1))
                .paid(true)
                .build());
        cancelUnpaidOrders.cancelUnpaidOrders();

        assertFalse(orderRepository.findById(orderEntity.getId()).isPresent());
        assertTrue(orderRepository.findById(orderEntity2.getId()).isPresent());
        assertTrue(orderRepository.findById(orderEntity3.getId()).isPresent());

    }
}
